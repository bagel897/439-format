# Formatter/Linter config

Notes:

- I am not responsible for your grades. At a minimum, test this with your own toolchain against a file (like test.c) for the [style guide](https://www.cs.utexas.edu/~ans/classes/cs439/projects/CStyleGuide.html)

## Requirements

- LLVM/clang toolchain (I ran this against clang 14). For vscode this is the [clangd extension](https://www.cs.utexas.edu/~ans/classes/cs439/projects/CStyleGuide.html).
- For neovim native lsp, use clangd and refer to [this](https://github.com/williamboman/nvim-lsp-installer/discussions/392)

## Autoformat (clang-format)

Code Layout:

- [x] Indent (3 spaces)
- [x] No tabs
- [ ] Line limit of 80 (sort of)
- [x] Blank lines before/after functions (somewhat)
- [x] Whitespace

## Lint (clang-tidy)

- [ ] Line limit
- [ ] Comments
- [x] Naming functions
- [x] Naming variables
- [x] Naming defines
- [ ] Naming structs
- [x] Meaningful names (Of course, to the best of its ability)
- [x] Initializing Pointers (and variables in general)
- [ ] Checking return values of system calls
